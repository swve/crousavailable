
require("dotenv").config();
const puppeteer = require("puppeteer-extra");
const util = require("./utils/utils");
const ms = require('ms');

const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

puppeteer.launch({ headless: true }).then(async (browser) => {
  console.log("Le script va demarrer dans quelques instants ..");

  /**
   *
   * Set you interval in english : 3s/20m etc
   */

   setInterval(async () => {
    util.findMeSomeHouses(browser)
   }, ms('30s'));
});
