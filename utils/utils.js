const axios = require("axios");

/**
 * Project utils
 *
 */

const findMeSomeHouses = async (browser) => {
  let result = await scrapWebPage(browser);
  if ((result = [])) {
    sendBotMessage("Pas d'offres de logement disponibles actuellement 😢");
  } else {
    sendBotMessage("Nouvelle mise à jour arrivée ✨");
    sendBotMessage(result.toString());
  }
  return result;
};

const scrapWebPage = async (browser) => {
  console.log("Running tests..");
  const page = await browser.newPage();
  await page.goto(process.env.CROUS_PAGE);
  await page.waitForTimeout(5000);

  let result = await page.evaluate(() => {
    var housingsArray = [];
    let bruteList = document.querySelectorAll(".SearchResults-item");
    for (var item of bruteList) {
      housingsArray.push([
        item.children[0].children[1].children[1].children[0].children[0]
          .children[0].innerText,
        item.children[0].children[1].children[1].children[0].children[0]
          .children[1].innerText,
        item.children[0].children[1].children[1].children[0].children[0]
          .children[2].innerText,
        item.children[0].children[1].children[1].children[0].children[0]
          .children[3].innerText,
        item.children[0].children[1].children[1].children[0].children[0]
          .children[4].innerText,
      ]);
    }
    return housingsArray;
    
  });
  console.log("Alert sent...");
  return result;
};

const sendBotMessage = (message) => {
  const url = "https://api.telegram.org/bot";
  const apiToken = process.env.TELEGRAM_BOT_KEY;
  const chatId = process.env.TELEGRAM_CHAT_ID;
  axios
    .post(`${url}${apiToken}/sendMessage`, {
      chat_id: chatId,
      text: message,
    })
    .then((response) => {
      //console.log(response);
    })
    .catch((error) => {
      //console.log(error);
    });
};

exports.sendBotMessage = sendBotMessage;
exports.scrapWebPage = scrapWebPage;
exports.findMeSomeHouses = findMeSomeHouses;
